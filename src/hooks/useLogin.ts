import { useCallback } from 'react'
import { BASE_URL, LOGIN_URL } from '../api/apiUrl'
import useUser from './useUser'

export type LoginProvider = 'google' | 'microsoft' | 'facebook'

export const useLogin = () => {
  const { refetch } = useUser()

  const handleLogin = useCallback(
    (provider: LoginProvider) => {
      const newWindow = window.open(
        `${BASE_URL}/${LOGIN_URL}/${provider}`,
        '_blank',
        'width=300,height=400'
      )

      const bc = new BroadcastChannel('auth_channel')
      bc.onmessage = (event) => {
        if (event.data === 'authentication complete') {
          if (newWindow) newWindow.close()
          refetch()
        }
      }
    },
    [refetch]
  )

  return handleLogin
}
