import { LoginResponse } from '../interfaces/axios'
import { TokenDecode } from '../interfaces/token'
import { useAuthState } from '../store'
import axiosInstance from './api.service'
import { decodeJwt } from './auth.service'

class UserService {
  async fetchUser(): Promise<TokenDecode | LoginResponse> {
    try {
      const response = await axiosInstance.get<LoginResponse>('auth/user', {
        withCredentials: true,
      })
      if (response.data.token) {
        const decoded = decodeJwt(response.data.token)
        useAuthState.getState().setToken(response.data.token)
        useAuthState.getState().setUser(decoded)
        return decoded
      }
      return response.data
    } catch (error) {
      console.log('Error fetching user', error)
      useAuthState.getState().setUser(null)
      useAuthState.getState().setToken(null)
      throw error
    }
  }
}

export const userService = new UserService()
