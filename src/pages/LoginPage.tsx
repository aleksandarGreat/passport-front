import { FC, useEffect } from 'react'
import {
  Button,
  VStack,
  Box,
  Icon,
  Card,
  CardHeader,
  CardBody,
  Text,
  Divider,
  CardFooter,
} from '@chakra-ui/react'
import { FaGoogle, FaMicrosoft, FaFacebook } from 'react-icons/fa'
import { useNavigate } from 'react-router-dom'
import { jwtDecode } from 'jwt-decode'
import { useLogin } from '../hooks/useLogin'
import { useAuthState } from '../store'
import { isAuthenticated } from '../services/auth.service'
import { ROUTES } from '../routes/pageRoutes'
import { AuthProvider } from '../interfaces/authProvider'

export const LoginPage: FC = () => {
  const handleLogin = useLogin()
  const navigate = useNavigate()
  const { setUser, token } = useAuthState()
  const isUserAuthenticated = isAuthenticated()

  useEffect(() => {
    if (token) {
      setUser(jwtDecode(token))
      navigate(ROUTES.LOGIN_SUCCESS)
    }
  }, [isUserAuthenticated, navigate, setUser, token])

  return (
    <Box
      w="100%"
      mx="auto"
      pt="100px"
      display="flex"
      flexDirection="column"
      justifyContent="center"
      alignItems="center"
    >
      <Card w="md" maxW="md">
        <CardHeader>
          <Text fontSize="xl" textAlign="center">
            Choose login provider
          </Text>
          <Divider />
        </CardHeader>
        <CardBody>
          <VStack spacing={4} align="center">
          <Button
              leftIcon={<Icon as={FaFacebook} color="#3C5A99" />}
              colorScheme="gray"
              minW="250px"
              minH="100px"
              onClick={() => handleLogin(AuthProvider.FACEBOOK)}
            >
              Login with Facebook
            </Button>
            <Button
              leftIcon={<Icon as={FaGoogle} color="#DB4437" />}
              colorScheme="gray"
              minW="250px"
              minH="100px"
              onClick={() => handleLogin(AuthProvider.GOOGLE)}
            >
              Login with Google
            </Button>
            <Button
              leftIcon={<Icon as={FaMicrosoft} color="#2B5797" />}
              colorScheme="gray"
              minW="250px"
              minH="100px"
              onClick={() => handleLogin(AuthProvider.MICROSOFT)}
            >
              Login with Microsoft
            </Button>
          </VStack>
        </CardBody>
      </Card>
    </Box>
  )
}
