import { AuthProvider } from "./authProvider"

export interface TokenDecode {
  email: string
  name: string
  profilePicture?: string
  provider: AuthProvider
}
