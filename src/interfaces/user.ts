import { AuthProvider } from "./authProvider"

export interface User {
  email: string
  name?: string
  provider: AuthProvider
  profilePicture?: string
}
