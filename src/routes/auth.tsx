import { RouteObject } from 'react-router-dom'
import { Layout } from '../components/ui/Layout'
import { ROUTES } from './pageRoutes'
import { LoginErrorPage } from '../pages/LoginErrorPage'
import { LoginPage } from '../pages/LoginPage'
import { LoginSuccessPage } from '../pages/LoginSuccessPage'

export const loginRoute: RouteObject = {
  path: ROUTES.DEFAULT_ROUTE,
  element: <Layout />,
  children: [
    {
      path: ROUTES.LOGIN_SUCCESS,
      element: <LoginSuccessPage />,
    },
    {
      path: ROUTES.DEFAULT_ROUTE,
      element: <LoginPage />,
    },
    {
      path: ROUTES.LOGIN,
      element: <LoginPage />,
    },
    {
      path: ROUTES.LOGIN_ERROR,
      element: <LoginErrorPage />,
    },
  ],
}
