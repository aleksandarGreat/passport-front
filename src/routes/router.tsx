import { createBrowserRouter } from 'react-router-dom'
import { NotFoundPage } from '../pages/NotFoundPage'
import { loginRoute } from './auth'
import { protectedRoute } from './protected'

const routes = [
  loginRoute,
  protectedRoute,
  {
    path: '*',
    element: <NotFoundPage />,
  },
]

export const router = createBrowserRouter(routes)
