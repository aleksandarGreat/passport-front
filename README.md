# Passport Login Front

This documentation covers the frontend part of the full-stack web application developed using Express.js, Node.js, React.js, and TypeScript. The frontend application incorporates social login functionality and displays user information obtained from the social login providers.

# Technologies Used
- React.js
- TypeScript

## Packages used
- @chakra-ui/react: A set of accessible and composable React components for building user interfaces.
- @emotion/react: Library for writing CSS styles with JavaScript.
- @emotion/styled: Styled-components for Emotion.
- axios: Promise-based HTTP client for making requests to the backend server.
- framer-motion: Library for creating animations in React.
- jwt-decode: Library for decoding JWTs (JSON Web Tokens).
- react: JavaScript library for building user interfaces.
- react-dom: Package providing DOM-specific methods for React.
- react-icons: Library providing a set of high-quality icons for React applications.
- react-query: Library for managing server-state in React applications.
- react-router-dom: Library for declarative routing in React applications.
- zustand: Library for creating small, fast, and reactive state management in React applications.

## Features

### Login Page
The login page provides users with a modern and responsive interface to log in via various social login providers. Users can choose to log in using Google, Microsoft, or other selected social logins.

### User Dashboard
Upon successful authentication, users are redirected to the user dashboard. The dashboard displays the user's email address and specific details retrieved from the social login profile. This includes details such as the user's name, profile picture, and any other relevant information provided by the social login provider.

## Getting Started
To run the frontend application locally, follow these steps:

1. Clone repository
2. Navigate to the frontend directory
```
cd <project_name>
```
3. Install dependencies
```
npm install
```
4. Start the development server
```
npm run dev
```

## Prerequisites
Do not forget to register your BE application with one of the provieders(Google, Facebook, Microsoft).

* [Facebook Developers](https://developers.facebook.com/)
* [Microsoft Azure](https://portal.azure.com/#home/)
* [Google Cloud](https://console.cloud.google.com/)

## Available Scripts
- `npm run dev`: Starts the development server.
- `npm run build`: Builds the application for production.
- `npm run lint`: Lints the codebase.
- `npm run preview`: Previews the production build.

## Potential new features
- Editing of user profiles that are stored in local DB, the cloud porivers values would not be updated.

## Contact
If you have any questions or suggestions, feel free to contact me:
[Email](mailto:aleksandar.radosevic@nqode.io)